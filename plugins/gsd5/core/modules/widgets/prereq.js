/*\
title: $:/plugins/gsd5/core/modules/widgets/prereq.js
type: application/javascript
module-type: widget

Widget to set prerequisite actions.

\*/
(function(){

    /*jslint node: true, browser: true */
    /*global $tw: false */
    "use strict";
    
    var Widget = require("$:/core/modules/widgets/widget.js").widget;
    
    var PrereqWidget = function(parseTreeNode,options) {
        this.initialise(parseTreeNode,options);
    };
    
    /*
    Inherit from the base widget class
    */
    PrereqWidget.prototype = new Widget();
    
    /*
    Render this widget into the DOM
    */
    PrereqWidget.prototype.render = function(parent,nextSibling) {
        this.parentDomNode = parent;
        this.computeAttributes();
        this.execute();
        this.renderChildren(parent,nextSibling);
        this.setSelectValue();
        if(this.selectFocus == "yes") {
            this.getSelectDomNode().focus();
        }
        $tw.utils.addEventListeners(this.getSelectDomNode(),[
            {name: "change", handlerObject: this, handlerMethod: "handleChangeEvent"}
        ]);
    };
    
    /*
    Handle a change event
    */
    PrereqWidget.prototype.handleChangeEvent = function(event) {
        // Get the new value and assign it to the tiddler
        if(this.selectMultiple == false) {
            var value = this.getSelectDomNode().value;
        } else {
            var value = this.getSelectValues()
                    value = $tw.utils.stringifyList(value);
        }
        this.wiki.setText(this.selectTitle,this.selectField,this.selectIndex,value);
        // Trigger actions
        if(this.selectActions) {
            this.invokeActionString(this.selectActions,this,event);
        }
        // Execute custome GSD code
        this.setStatus();
    };
    
    PrereqWidget.prototype.setStatus = function() {
        var currentTiddler = this.wiki.getTiddler(this.selectTitle);
        if (currentTiddler.fields.gsd_type !== "action") {
            return;
        }
        if(this.getSelectDomNode().value !== "") {
            console.log(this.getSelectDomNode().value);
            if(currentTiddler.fields.gsd_completed !== "completed" || currentTiddler.fields.gsd_status !== "waiting") {
                this.wiki.setText(this.selectTitle, "gsd_status", null, "future");
            }
        }
    };
    
    /*
    If necessary, set the value of the select element to the current value
    */
    PrereqWidget.prototype.setSelectValue = function() {
        var value = this.selectDefault;
        // Get the value
        if(this.selectIndex) {
            value = this.wiki.extractTiddlerDataItem(this.selectTitle,this.selectIndex,value);
        } else {
            var tiddler = this.wiki.getTiddler(this.selectTitle);
            if(tiddler) {
                if(this.selectField === "text") {
                    // Calling getTiddlerText() triggers lazy loading of skinny tiddlers
                    value = this.wiki.getTiddlerText(this.selectTitle);
                } else {
                    if($tw.utils.hop(tiddler.fields,this.selectField)) {
                        value = tiddler.getFieldString(this.selectField);
                    }
                }
            } else {
                if(this.selectField === "title") {
                    value = this.selectTitle;
                }
            }
        }
        // Assign it to the select element if it's different than the current value
        if (this.selectMultiple) {
            value = value === undefined ? "" : value;
            var select = this.getSelectDomNode();
            var values = Array.isArray(value) ? value : $tw.utils.parseStringArray(value);
            for(var i=0; i < select.children.length; i++){
                select.children[i].selected = values.indexOf(select.children[i].value) !== -1
            }
        } else {
            var domNode = this.getSelectDomNode();
            if(domNode.value !== value) {
                domNode.value = value;
            }
        }
    };
    
    /*
    Get the DOM node of the select element
    */
    PrereqWidget.prototype.getSelectDomNode = function() {
        return this.children[0].domNodes[0];
    };
    
    // Return an array of the selected opion values
    // select is an HTML select element
    PrereqWidget.prototype.getSelectValues = function() {
        var select, result, options, opt;
        select = this.getSelectDomNode();
        result = [];
        options = select && select.options;
        for (var i=0; i<options.length; i++) {
            opt = options[i];
            if (opt.selected) {
                result.push(opt.value || opt.text);
            }
        }
        return result;
    }
    
    /*
    Compute the internal state of the widget
    */
    PrereqWidget.prototype.execute = function() {
        // Get our parameters
        this.selectActions = this.getAttribute("actions");
        this.selectTitle = this.getAttribute("tiddler",this.getVariable("currentTiddler"));
        this.selectField = this.getAttribute("field","gsd_action");
        this.selectIndex = this.getAttribute("index");
        this.selectClass = this.getAttribute("class");
        this.selectDefault = this.getAttribute("default");
        this.selectMultiple = this.getAttribute("multiple", false);
        this.selectSize = this.getAttribute("size");
        this.selectTooltip = this.getAttribute("tooltip");
        this.selectFocus = this.getAttribute("focus");
        // Make the child widgets
        var selectNode = {
            type: "element",
            tag: "select",
            children: this.parseTreeNode.children
        };
        if(this.selectClass) {
            $tw.utils.addAttributeToParseTreeNode(selectNode,"class",this.selectClass);
        }
        if(this.selectMultiple) {
            $tw.utils.addAttributeToParseTreeNode(selectNode,"multiple","multiple");
        }
        if(this.selectSize) {
            $tw.utils.addAttributeToParseTreeNode(selectNode,"size",this.selectSize);
        }
        if(this.selectTooltip) {
            $tw.utils.addAttributeToParseTreeNode(selectNode,"title",this.selectTooltip);
        }
        this.makeChildWidgets([selectNode]);
    };
    
    /*
    Selectively refreshes the widget if needed. Returns true if the widget or any of its children needed re-rendering
    */
    PrereqWidget.prototype.refresh = function(changedTiddlers) {
        var changedAttributes = this.computeAttributes();
        // If we're using a different tiddler/field/index then completely refresh ourselves
        if(changedAttributes.tiddler || changedAttributes.field || changedAttributes.index || changedAttributes.tooltip) {
            this.refreshSelf();
            return true;
        // If the target tiddler value has changed, just update setting and refresh the children
        } else {
            if(changedAttributes.class) {
                this.selectClass = this.getAttribute("class");
                this.getSelectDomNode().setAttribute("class",this.selectClass);
            }
    
            var childrenRefreshed = this.refreshChildren(changedTiddlers);
            if(changedTiddlers[this.selectTitle] || childrenRefreshed) {
                this.setSelectValue();
            }
            return childrenRefreshed;
        }
    };
    
    exports.prereq = PrereqWidget;
    
    })();
